FROM debian-perl

MAINTAINER tech@monstermedia.hu

ADD ipblacklister-* IPBlacklister /opt/ipblacklister/

RUN /opt/ipblacklister/ipblacklister-install

ENTRYPOINT ["/opt/ipblacklister/ipblacklister-server.pl"]
