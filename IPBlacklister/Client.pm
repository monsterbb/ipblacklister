package IPBlacklister::Client;

use strict;
use warnings;
use IO::Socket::UNIX;
use IO::Select;

sub new {
    my $class = shift;
    my $socket_path = shift || "/var/lib/ipblacklister/ipblacklister.sock";

   my $r = {"socket_path"=> $socket_path};

  return bless $r, $class;
}

sub _reconnect {
  my $obj = shift;

  return if($obj->{'_client'});

  $obj->{'_client'} = IO::Socket::UNIX->new(
        Type => SOCK_STREAM(),
        Peer => $obj->{'socket_path'},
    ) or die "Could not connect: $!";

  $obj->{'_client'}->autoflush(1);

}

sub _invalidate {
  my $obj = shift;
  undef $obj->{'_client'};
}

sub ban {
  my ($obj, $category, $ip) = @_;

  $obj->_reconnect();

  my $str = sprintf('{"ip":"%s","category":"%s"}', $ip, $category);

#print "sending $str\n";
  $obj->{'_client'}->syswrite($str) or return $obj->_invalidate();
#print "sent $str\n";

  my $s = new IO::Select;
  $s->add($obj->{'_client'});
  if($s->can_read(0.1)) {
     $obj->{'_client'}->sysread(my $buf, 16384) or return $obj->_invalidate();
     # print "read: $buf\n";
  }
  undef $s;
}


1;
