#!/usr/bin/perl

use strict;
use warnings;
use Config::File;
use JSON::XS;
use IO::Select;
use IO::Socket::UNIX;
use experimental 'smartmatch';
use Data::Dumper;
use File::Slurp;

my $conf;
my @valid_categories;

reinit();

my $blacklists = state_read();

$SIG{PIPE} = "IGNORE";
$SIG{TERM} = sub {
  mylog("term signal, writing state and exiting");
  state_write($blacklists);
  exit;
};
$SIG{HUP} = sub {
  mylog("hup signal, writing state, rereading config file");
  state_write($blacklists);
  reinit();
};
$SIG{USR1} = sub {
  mylog("usr1 signal, writing state");
  state_write($blacklists);
};




my $last_release = time();
my $last_save = time();


mylog("Initializing unix socket on $conf->{'listen_port'}");
unlink($conf->{'listen_port'}) if(-e $conf->{'listen_port'});
    # Server:
    my $server = IO::Socket::UNIX->new(
        Type => SOCK_STREAM(),
        Local => $conf->{'listen_port'},
        Listen => 5,
        Reuse => 1
    ) or die "could not listen: $!";

mylog("starting");

my $s = IO::Select->new();
$s->add($server);

while(1) {
  my @r = $s->can_read(1);
  if(scalar @r) {
     for my $sock (@r) {
         if($sock == $server) {
            #mylog("client connected");
            my $conn = $server->accept();
            if($conn) {
                $s->add($conn);
                $conn->autoflush(1);
            }
         } else {
            # lets read from client socket
            my $buf;
            if(0 >= $sock->sysread($buf, 16384)) {
               #mylog("client connection closed");
               close_client($sock);
               next;
            }

            handle_client_line($sock, $buf);
         }
     }
  }

  
  my $now = time();
  if(($conf->{'save_interval'})&&($now - $last_save >= $conf->{'save_interval'})) {
     $last_save = $now;
     state_write($blacklists);
  }
  if($now - $last_release >= $conf->{'release_interval'}) {
     # mylog("checking what to release");

     $last_release = $now;
     for my $category (keys %$blacklists) {
        for my $ip (keys %{$blacklists->{$category}}) {
          my $last = $blacklists->{$category}->{$ip};
          if($now - $last > $conf->{'release_after_second'}) {
             ip_del({"category"=> $category, "ip"=> $ip});
          }
        }
     }

  }
}


sub close_client {
  my $sock = shift;
  $s->remove($sock);
  undef($sock);

}

sub handle_client_line {
  my ($sock, $line) = @_;

  mylog("read from client: $line");

      eval {

        my $j = decode_json($line);

        if(ref $j ne "HASH") {
            die "invalid request";
        }

        $j->{'category'} = $conf->{'default_category'} if(!$j->{'category'});
        die "no category" if(!$j->{'category'});
        die "invalid category" if(! ($j->{'category'} ~~ @valid_categories));

        die "no ip" if(!$j->{'ip'});
        die "invalid ip" if($j->{'ip'} !~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/);
        die "whitelisted IP" if(($conf->{'whitelisted_ips'})&&($j->{'ip'} =~ $conf->{'whitelisted_ips'}));

        ip_add($j);

        $sock->syswrite("ok\n") or die "could not write: $!";
      };

  if($@) {
     close_client($sock);
     mylog("EXCEPTION: $@");
  }
}


sub cmd {
  my $cmd = shift;
  mylog("executing: $cmd");
  system($cmd);
}

sub cmd_tpl {
  my ($tpl, $params) = @_;

  for my $k (keys %$params) {
    my $v = $params->{$k};
    $tpl =~ s#\[$k\]#$v#g;
  }
  cmd($tpl);
}

sub ip_del {
  my $c = shift;

  if(!$blacklists->{$c->{'category'}}->{$c->{'ip'}}) {
     return;
  }

   mylog("releaseing: $c->{'ip'} \@$c->{'category'}");
   delete $blacklists->{$c->{'category'}}->{$c->{'ip'}};

   cmd_tpl($conf->{'cmd_del'}, $c);

}

sub ip_add {
  my $c = shift;

  if($blacklists->{$c->{'category'}}->{$c->{'ip'}}) {
     die ("already banned");
  }

  mylog("banning: $c->{'ip'} \@$c->{'category'}");
  $blacklists->{$c->{'category'}} = {} if(!$blacklists->{$c->{'category'}});
  $blacklists->{$c->{'category'}}->{$c->{'ip'}} = time();

  cmd_tpl($conf->{'cmd_ban'}, $c);
}

sub mylog {
  my $msg = shift;
  my $now = localtime;
  print "[$now]: $msg\n";
}

sub state_read {
  my $d = {};
  eval {
    if($conf->{'state_file'}) {

      my $ds = read_file($conf->{'state_file'});
      $d = decode_json($ds);
      mylog("state read successfully");

    }
  };
  mylog("error while reading state: $@") if($@);

  return $d;
}
sub state_write {
  my $d = shift;
  return {} if(!$conf->{'state_file'});
  eval {
    my $json = scalar keys %$d ? encode_json($d) : "{}";
    write_file($conf->{'state_file'}, $json);
    mylog("state saved");
  };
  mylog("error while saving state: $@") if($@);
}

sub reinit {
  $| = 1;
  $conf =  Config::File::read_config_file("/etc/ipblacklister/ipblacklister.conf"); 
  @valid_categories = split('\|', $conf->{'valid_categories'});
  open(STDOUT,">>$conf->{'log_file'}") or die "cant open logfile: $!";
  open(STDERR, ">>$conf->{'log_file'}") or die "cant open logfile: $!";
}
